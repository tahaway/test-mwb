import React, { Component, Suspense } from 'react';
import { connect } from 'react-redux';
import socketIOClient from "socket.io-client";
import {
  BrowserRouter as Router,
  Route,
  Switch,
  Redirect
} from 'react-router-dom';
import { IntlProvider } from 'react-intl';
import {
  updateUserSocket,
  getProductItemUpdate,
  getCartProductList
} from "./redux/actions";
import appLocale from './lang';
import NotificationContainer from './components/common/react-notifications/NotificationContainer';
import { AuthRoute } from './AuthRoute';
import { registerLocale, setDefaultLocale } from  "react-datepicker";
import et from 'date-fns/locale/et';
import enUS from 'date-fns/locale/en-GB';
registerLocale('et', et);
registerLocale('en', enUS);

const ViewMain = React.lazy(() =>
  import(/* webpackChunkName: "views" */ './views')
);
const Viewapp = React.lazy(() =>
  import(/* webpackChunkName: "views-app" */ './views/app')
);
const ViewUser = React.lazy(() =>
  import(/* webpackChunkName: "views-user" */ './views/user')
);
const ViewError = React.lazy(() =>
  import(/* webpackChunkName: "views-error" */ './views/error')
);


class app extends Component {

  componentDidMount(){
    this.socketHandler()
  }

  socketHandler (){

    if(!this.props.authUser.socket){

    const socket = socketIOClient('localhost:3005',{
      query: { token :this.props.loginUser } ,
      upgrade: false,
      transports: ['websocket'],
      secure: true,
    });
    
    this.props.updateUserSocket(socket);
    socket.emit('subscribe','cart');

    socket.on("updateProduct", data => {
      console.log("updateProduct",data);
      this.props.getProductItemUpdate(data)
    });

    socket.on("updateCart", data => {
      console.log("Update Cart Request",data);
      this.props.getCartProductList(this.props.history)
    });

    }
  }


  render() {
    const { locale, loginUser } = this.props;
    const currentappLocale = appLocale[locale];
    setDefaultLocale(locale);
    return (
      <div className="h-100">
        <IntlProvider
          locale={currentappLocale.locale}
          messages={currentappLocale.messages}
        >
          <React.Fragment>
            <NotificationContainer />
            <Suspense fallback={<div className="loading" />}>
              <Router>
                <Switch>
                  <AuthRoute
                    path="/app"
                    authUser={loginUser}
                    component={Viewapp}
                  />
                  <Route
                    path="/user"
                    render={props => <ViewUser {...props} />}
                  />
                  <Route
                    path="/error"
                    exact
                    render={props => <ViewError {...props} />}
                  />
                  <Route
                    path="/"
                    exact
                    render={props => <ViewMain {...props} />}
                  />
                  <Redirect to="/error" />
                </Switch>
              </Router>
            </Suspense>
          </React.Fragment>
        </IntlProvider>
      </div>
    );
  }
}

const mapStateToProps = ({ authUser, settings, cartapp }) => {
  const { user: loginUser } = authUser;
  const { locale } = settings;
  return { loginUser, locale, cartapp, authUser};
};

export default connect(
  mapStateToProps,
  { updateUserSocket,
  getProductItemUpdate,
  getCartProductList }
)(app);
