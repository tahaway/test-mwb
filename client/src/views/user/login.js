import React, { Component } from "react";
import { Row, Card, CardTitle, Label, FormGroup, Button } from "reactstrap";
import { NavLink } from "react-router-dom";
import { connect } from "react-redux";
import { Dropdown } from 'react-bootstrap';
import { NotificationManager } from "../../components/common/react-notifications";
import { Formik, Form, Field, } from "formik";
import { loginUser } from "../../redux/actions";
import { Colxx } from "../../components/common/CustomBootstrap";
import IntlMessages from "../../helpers/IntlMessages";


class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      password: "",
      portal : ""
    };
  }

  onUserLogin = (values) => {
    if (!this.props.loading) {
      if (values.username !== "" && values.password !== "") {
        this.props.loginUser(values, this.props.history);
      }
    }
  }

  changeSelect = select => {
      this.setState({
        ...this.state,
        portal : select.currentTarget.value
      });
  }

  validateUsername = (value) => {
    let error;
    if (!value) {
      error = <IntlMessages id="view.login.errorUsername" />;
    } else if (value.length < 5) {
      error = <IntlMessages id="view.login.errorUsernameLimit" />;
    }
    return error;
  }

  validatePortal = (value) => {
    let error;
    if (value === 's' | value === 'i') {
    } else {
      error = <IntlMessages id="view.login.validPortal" />;
    }
    return error;
  }


  validatePassword = (value) => {
    let error;
    if (!value) {
      error = <IntlMessages id="view.login.passwordUsername" />;
    } else if (value.length < 5) {
      error = <IntlMessages id="view.login.errorerrorPasswordLimit" />;
    }
    return error;
  }

  componentDidUpdate() {
    if (this.props.error) {
      NotificationManager.warning(
        this.props.error,
        "Login Error",
        3000,
        null,
        null,
        ''
      );
    }
  }

  changeLanguage = lang => {
    localStorage.setItem('currentLanguage',lang);
    window.location.reload(false);
  }

  render() {
    const { password, username,portal } = this.state;
    const initialValues = {username, password, portal};
    
    return (
      <Row className="h-100">
        <Colxx xxs="12" md="10" className="mx-auto my-auto">
          <Card className="auth-card">
            <div className="position-relative image-side ">
            <NavLink to={`/`} className="white">
              <img alt="app.ee" className="logoPNG" src="/assets/img/master-logo-white.png" />
            </NavLink>

            </div>
            <div className="form-side">
              <div className="dropdown float-right">
                      <Dropdown>
                      <Dropdown.Toggle 
                      variant="secondary btn-sm" 
                      id="dropdown-basic">
                          <IntlMessages id="language.title" />
                      </Dropdown.Toggle>

                      <Dropdown.Menu >
                          <Dropdown.Item onClick={ ()=> this.changeLanguage('en') } ><IntlMessages id="language.en" /></Dropdown.Item>
                          <Dropdown.Item onClick={ ()=> this.changeLanguage('et') }><IntlMessages id="language.et" /></Dropdown.Item>
                      </Dropdown.Menu>
                      </Dropdown>
              </div>
              <div className="margin-bottom-100"></div>
              <CardTitle className="mb-4">
                <IntlMessages id="user.login-title" />
              </CardTitle>

              <Formik
                initialValues={initialValues}
                onSubmit={this.onUserLogin}>
                {({ errors, touched }) => (
                  <Form className="av-tooltip tooltip-label-bottom">
                    <FormGroup className="form-group has-float-label">
                      <Label>
                        <IntlMessages id="user.username" />
                      </Label>
                      <Field
                        className="form-control"
                        name="username"
                        type="email"
                        validate={this.validateUsername}
                      />
                      {errors.username && touched.username && (
                        <div className="invalid-feedback d-block">
                          {errors.username}
                        </div>
                      )}
                    </FormGroup>
                    <FormGroup className="form-group has-float-label">
                      <Label>
                        <IntlMessages id="user.password" />
                      </Label>
                      <Field
                        className="form-control"
                        type="password"
                        name="password"
                        validate={this.validatePassword}
                      />
                      {errors.password && touched.password && (
                        <div className="invalid-feedback d-block">
                          {errors.password}
                        </div>
                      )}
                    </FormGroup>
                      <div className="d-flex justify-content-between align-items-center">
               
                      <Button
                        color="primary"
                        className={`btn-shadow btn-multiple-state ${this.props.loading ? "show-spinner" : ""}`}
                        size="lg"
                      >
                        <span className="spinner d-inline-block">
                          <span className="bounce1" />
                          <span className="bounce2" />
                          <span className="bounce3" />
                        </span>
                        <span className="label"><IntlMessages id="user.login-button" /></span>
                      </Button>
                    </div>


                  </Form>
                )}
              </Formik>
            </div>
          </Card>
        </Colxx>
      </Row>
    );
  }
}
const mapStateToProps = ({ authUser }) => {
  const { user, loading, error } = authUser;
  return { user, loading, error };
};

export default connect(
  mapStateToProps,
  {
    loginUser
  }
)(Login);
