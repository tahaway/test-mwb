import appLocaleData from 'react-intl/locale-data/en';
import enMessages from '../locales/en_GB';

const EnLang = {
    messages: {
        ...enMessages
    },
    locale: 'en-GB',
    data: appLocaleData
};
export default EnLang;