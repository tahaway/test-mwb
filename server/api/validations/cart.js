const {
    code400,
    code401,
    code409,
    code200GetCart
} = require('../../config/Swagger')

const validateGetCart = {
    schema: {
        description: 'Get user cart items list.',
        tags: ['cart'],
        response: {
            400: code400,
            401: code401,
            200: code200GetCart,
            409: code409
        }
    }
}

const validateUpdateCart = {
    schema: {
        description: 'Update user cart item list',
        tags: ['cart'],
        params: {
            type: 'object',
            properties: {
                action: {
                    type: 'string',
                    enum: ['addition', 'subtract'],
                    default: 'addition',
                },
                id: {
                    type: 'string',
                    min: 10,
                },
            },
            required: ['action', 'id'],
        },
        response: {
            400: code400,
            401: code401,
            200: code200GetCart,
            409: code409
        }
    }
}


module.exports = {
    validateGetCart,
    validateUpdateCart
}