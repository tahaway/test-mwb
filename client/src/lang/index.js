import { addLocaleData } from 'react-intl';
import enLang from './entries/en-GB';
import etLang from './entries/et-EE';
import enRtlLang from './entries/en-US-rtl';

const appLocale = {
    en: enLang,
    et: etLang,
    enrtl:enRtlLang
};
addLocaleData(appLocale.en.data);
addLocaleData(appLocale.et.data);
addLocaleData(appLocale.enrtl.data);

export default appLocale;
