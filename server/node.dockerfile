FROM node:slim
WORKDIR /usr/src/app
RUN  npm install 
RUN  npm install -g nodemon
COPY . .
EXPOSE 3005
CMD ["nodemon", "app.js"]

