
const ErrorHandler = (error) => {
    console.log("info",error);
    if(error.response.data.error === 'Unauthorized'){
        localStorage.removeItem('token');
        return false
    }else{
        return error;
    }
}

export default ErrorHandler;

