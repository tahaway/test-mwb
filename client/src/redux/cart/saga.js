import { all, call, fork, put, takeEvery } from "redux-saga/effects";

import {
    CART_DATA,
	PRODUCTS_DATA,
	CART_ITEM_UPDATE_DATA
} from "../actions";

import {
  getCartSuccess,
  getCartError,
  getCartProductsSuccess,
  getCartProductsError,
  getProductItemUpdate,
  selectedProduct
} from "./actions";
import Request  from '../api/request';
import ErrorHandler  from '../api/remove';


function* getCartList({ payload }) {
  	try {
		let uri = '/v1/api/cart/';
		const response = yield call(Request,uri,'get');
		yield put(getCartSuccess(response.data));
	} catch (error) {
		if(!ErrorHandler(error)){
			if(payload.history) payload.history.push('/user/login'); 
		} 
		yield put(getCartError(error));
	}

}

function* getProductList({ payload }) {
  	try {
		let uri = '/v1/api/product/list/1/100/';
		const response = yield call(Request,uri,'get');
		yield put(getCartProductsSuccess(response.data));
	} catch (error) {
		if(!ErrorHandler(error)){
			if(payload.history) payload.history.push('/user/login'); 
		} 
		yield put(getCartProductsError(error));
	}

}

function* updateCartItem({ payload }) {
  	try {
		const uri = '/v1/api/cart/'+ payload.action + '/' + payload.id + '/';
		const response = yield call(Request,uri,'get');
		const changes = { modal : true, item : { name : response.data.product[0].name, quantity : 1, action : payload.action } }
		payload.socket.emit("updateProduct", { data : response.data.product[0], changes : changes }) 	 ;
		yield put(selectedProduct(response.data.product[0]));
		yield put(getCartSuccess(response.data));
		yield put(getProductItemUpdate({ data : response.data.product[0] }));		
	} catch (error) {
		if(!ErrorHandler(error)){
			if(payload.history) payload.history.push('/user/login'); 
		} 
		yield put(getCartError(error));
	}

}



export function* watchGetCart() {
  yield takeEvery(CART_DATA, getCartList);
}

export function* watchGetCartProducts() {
  yield takeEvery(PRODUCTS_DATA, getProductList);
}

export function* watchUpdateCartITem() {
  yield takeEvery(CART_ITEM_UPDATE_DATA, updateCartItem);
}



export default function* rootSaga() {
	yield all([
		fork(watchGetCart),
		fork(watchGetCartProducts),
		fork(watchUpdateCartITem)
	]);
}