import React, { Component, Fragment } from "react";
import { Colxx } from "../../../components/common/CustomBootstrap";
import { Badge,Button } from "reactstrap";
import Item from "../../../components/list/Item";
import Breadcrumb from "../../../containers/navs/Breadcrumb";
import { connect } from "react-redux";
import { injectIntl } from "react-intl";
import {
  getCartList,
  getCartProductList,
  closeStockModal,
  selectedProduct
} from "../../../redux/actions";
import { Row } from "reactstrap";
import ModalUi from "../../../components/common/modal/ModalUi";
import ModalUiAlert from "../../../components/common/modal/ModalUIAlert";

class List extends Component {

    constructor(props) {
    super(props);
    this.state = {
        modal: false,
        id : false
    };
    }

    componentDidMount() {
      this.props.getCartProductList(this.props.history);
      this.props.getCartList(this.props.history);
    } 

    toggle = () => {
      this.setState(prevState => ({
        modal: !prevState.modal,
      }));
    };

    toggleSelect = (data) => {
      this.props.selectedProduct(data)
      this.setState(prevState => ({
        modal: !prevState.modal,
      }));
    };

    render() {
        const { products,cart, changes } = this.props.cartapp
        const totalSum = cart.quantity.reduce((a, b) => a + b, 0)

        return (
            <Fragment>
            <Row>
              <Colxx xxs="8">
                <Breadcrumb heading="menu.cart" match={this.props.match} />
              </Colxx>
              <Colxx xxs="4">
              <Button color="primary" className={"float-right"} outline>
                  Total in cart <Badge color="secondary">{totalSum}</Badge> item(s) 
              </Button>
              </Colxx>
            </Row>
            <Row>
            <ModalUi modal={this.state.modal} toggle={this.toggle}  />
            <ModalUiAlert data={changes} toggle={this.props.closeStockModal}  />
            {products && products.map((item,index)=> {
              return <Item item={ item } key={index} toggle={this.toggleSelect} index={index} />
            })}
            </Row>
          </Fragment>
        )
    }
}


const mapStateToProps = ({ cartapp }) => {
  return { cartapp };
};

export default injectIntl(
  connect(
    mapStateToProps,
    {
      getCartList,
      getCartProductList,
      closeStockModal,
      selectedProduct
    }
  )(List)
);
