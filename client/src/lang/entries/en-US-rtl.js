import appLocaleData from 'react-intl/locale-data/en';
import enMessages from '../locales/en_GB';

const EnLangRtl = {
    messages: {
        ...enMessages
    },
    locale: 'en-US',
    data: appLocaleData
};
export default EnLangRtl;