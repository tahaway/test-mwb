// import dependencies from npm
const Fastify = require('fastify');
const io = require('socket.io');
const path = require('path');
const AutoLoad = require('fastify-autoload');
const uuidv4 = require('uuid/v4');
const nconf = require('nconf');
const jwt = require('fastify-jwt');
const Users = require('./api/models/User');
const Cart = require('./api/models/Cart');
var jwtSocket = require('jwt-simple');
const createServer = (options) => {
    const {
        logSeverity
    } = options;
    const {
        HOST,
        PORT,
        JWTKEY
    } = process.env;

    // create the server
    const server = Fastify({
        ignoreTrailingSlash: true,
        useUnifiedTopology: true,
    });

    server.register(require('fastify-swagger'), {
        routePrefix: '/documentation',
        swagger: {
            host: HOST + ":" + PORT,
            info: {
                title: 'API',
                description: '',
                version: '1.0.0'
            },
            schemes: ['http'],
            consumes: ['application/json'],
            produces: ['application/json'],
            tags: [{
                    name: 'auth',
                    description: 'Authentication related end-points'
                },
                {
                    name: 'product',
                    description: 'products related end-points'
                },
                {
                    name: 'cart',
                    description: 'carts related end-points'
                },
            ],
            securityDefinitions: {
                Bearer: {
                    type: 'apiKey',
                    name: 'Authorization',
                    in: 'header'
                }
            }
        },
        exposeRoute: true
    })

    server.register(require('fastify-jwt'), {
        secret: JWTKEY,
    })

    server
        .decorate('asyncVerifyJWT', async function (request, reply, next) {
            try {
                await request.jwtVerify();
            } catch (err) {
                reply.send(err)
            }
        })
        .register(require('fastify-auth'));

    server.register(require('fastify-socket.io'), {
        // put your options here
        cors: {
            origin: '*',
            }
    })

    server.register(AutoLoad, {
        dir: path.join(__dirname, 'api', 'routes')
    });




    
    // start the server
    server.listen(PORT, HOST, (err) => {
        if (err) {
            server.log.error(err);
            console.log(err);
            process.exit(1);
        }
        console.log("Server is up at http://" + HOST + ":" + PORT);
        server.log.info('Server Started');
    });
}

module.exports = {
    createServer
}