import React, { Component } from "react";
import {
  getCartItemUpdate,
} from "../../../redux/actions";
import { connect } from "react-redux";
import { injectIntl } from "react-intl";
import {
  Button,
  ModalHeader,
  ModalBody,
  ModalFooter,
} from "reactstrap";

class ModalBodyUI extends Component {

  stockUpdate(type){
    this.props.getCartItemUpdate(type,this.props.cartapp.selectedProduct._id,this.props.authUser.socket,this.props.history);
  }

  checkItemCount(){
    const selectedRecord = this.props.cartapp.cart.items.indexOf(this.props.cartapp.selectedProduct._id);
    return parseInt( this.props.cartapp.cart.quantity[selectedRecord] ? this.props.cartapp.cart.quantity[selectedRecord] : 0 );
  }

  render() {
    const { toggle  } = this.props;
    const { selectedProduct : data } = this.props.cartapp;
   
    if(!data) return false

    const itemtCount = this.checkItemCount();
    return (
      <div>
        <ModalHeader toggle={toggle}>
            <strong>{data.name} - ${data.price}</strong>
        </ModalHeader>
        <ModalBody>
        <div style={{ height:100}}>
            <div className={"float-left"}>Product Info</div>
                <input disabled max={5} className={"form-control w-10 float-right"} value={data.stock.current} />
            </div>
            <div className={"text-right"}>
                { "this item in cart :" + itemtCount}
            </div>
        <div className={"text-right"}>
            <Button color="primary" onClick={()=> this.stockUpdate('addition') } className="default mb-2">
            Add to Cart
            </Button>
            {" "}
            <Button color="warning" onClick={()=> { if(itemtCount !== 0){ this.stockUpdate('subtract') } } } className="default mb-2">
            Remote from Carts
            </Button> 
        </div>
        </ModalBody>
        <ModalFooter> 
            <Button onClick={toggle} color="danger" className="default mb-2">
                Close
            </Button> 
        </ModalFooter>
        </div>
    );
  }
}

const mapStateToProps = ({ cartapp, authUser }) => {
  return { cartapp,authUser };
};

export default injectIntl(
  connect(
    mapStateToProps,
    {
        getCartItemUpdate
    }
  )(ModalBodyUI)
);