import React, { Component } from "react";
import { Modal } from "reactstrap";
import ModalBodyAlert from "./ModalBodyAlert";

export default class ModalUiAlert extends Component {

  render() {
    const { data, toggle } = this.props;
    return (
      <Modal isOpen={data.modal} toggle={toggle} >
            <ModalBodyAlert modal={data.modal} toggle={toggle} data={data.item}  />
      </Modal>
    );
  }
}
