const {
  code400,
  code401,
  code409,
  code200ProductList
} = require('../../config/Swagger')

const validateProductList = {
  schema: {
    description: 'Request to get all products.',
    tags: ['product'],
    params: {
      type: 'object',
      properties: {
        page: {
          type: 'number',
          min : 1,
          default: 1,
        },
        limit: {
          type: 'number',
          min : 10,
          default: 10,
        },
      },
      required: ['page', 'limit'],
    },
    response: {
      400: code400,
      401: code401,
      200: code200ProductList,
      409: code409
    }
  }
}


module.exports = {
  validateProductList,
}
