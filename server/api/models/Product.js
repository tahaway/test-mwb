const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const productSchema = new mongoose.Schema({
    _id: { type: Schema.ObjectId, auto: true },
    name : {type : String},
    price : {type : Number, default : 0 },
    stock : {
        current : {type : Number, default : 0, min: 0  },
        hold : {type : Number, default : 0, min:0 },
        taken : {type : Number, default : 0 }
    }
});

const Product = mongoose.model('products', productSchema);
module.exports = Product;