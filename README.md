
Step 1 :
Please download .env file from email, as repo is public we are testing the docker on desktop machine, * its contains Cloud Database credentials.

Step 2 :
git clone git@bitbucket.org:tahaway/test-mwb.git .

Step 3 : 
cd server 
copy downloaded .env file to server root path

Step 4 :
docker-compose build node

Step 5 :
docker-compose up

Step 6 : 
open new terminal, and go to client directory
cd client

Step 7 :
edit default .env file, change PROXYHOST= to your Local IP like 192.168.0.28, we are using Proxy Middleware to access REST API locally.

Step 8 :
docker-compose build react

Step 9 :
docker-compose up

Step 10 :
open http://localhost:3000


Two User Accounts.

Account : tahaway@gmail.com
Pass : 123456789

Account : tahawayes@gmail.com
Pass : 123456789 
