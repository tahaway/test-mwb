const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const cartSchema = new mongoose.Schema({
    _id: { type: Schema.ObjectId, auto: true },
    uid : { type: Schema.ObjectId },
    items : [{ type: Schema.ObjectId }],
    quantity : [{ type: Number }]
});

const Cart = mongoose.model('cart', cartSchema);
module.exports = Cart;