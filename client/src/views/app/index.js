import React, { Component, Suspense } from 'react';
import { Route, withRouter, Switch, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import {
  refreshToken,
} from "../../redux/actions";
import AppLayout from '../../layout/AppLayout';
import IdleTimer from 'react-idle-timer'

const Cart = React.lazy(() =>
  import(/* webpackChunkName: "viwes-app" */ './cart')
);

class App extends Component {
  
  constructor(props) {
    super(props);
    this.handleOnIdle = this.handleOnIdle.bind(this)

  }

  componentDidMount() {
    setInterval(async () => {
      if(this.props.history.pathname !== '/user/login'){
        this.props.refreshToken(this.props.history)
      }
    }, 100000);
  }

  handleOnIdle = (event) => {
    localStorage.setItem('token',false);
    this.props.history.push('/user/login');
  }

  render() {
    const { match } = this.props;
    
    return (
      <AppLayout>
        <div className="dashboard-wrapper">
          <IdleTimer
          ref={ref => { this.idleTimer = ref }}
          timeout={3600000}
          onIdle={this.handleOnIdle}
          debounce={250}
          />
          <Suspense fallback={<div className="loading" />}>
            <Switch>
              <Redirect exact from={`${match.url}/`} to={`${match.url}/cart`} />
              <Route
                path={`${match.url}/cart`}
                render={props => <Cart {...props} />}
              />
              <Redirect to="/error" />
            </Switch>
          </Suspense>
        </div>
      </AppLayout>
    );
  }
}
const mapStateToProps = ({ menu }) => {
  const { containerClassnames } = menu;
  return { containerClassnames };
};

export default withRouter(
  connect(
    mapStateToProps,
    { refreshToken }
  )(App)
);
