const {
  randomID,
  converForSchemaSwagger
} = require('../api/utils/general');
var md5 = require('md5');

const code400 = {
  description: 'Invalid body payload.',
  type: 'object',
  properties: {
    statusCode: {
      type: 'number'
    },
    error: {
      type: 'string'
    },
    status: {
      type: 'string'
    },
    message: {
      type: 'string'
    },
  },
  example: {
    statusCode: 400,
    error: 'Bad Request',
    status: 'fail',
    message: 'Body message error',
  }
};

const code401 = {
  description: 'Unauthorized response',
  type: 'object',
  properties: {
    statusCode: {
      type: 'number'
    },
    error: {
      type: 'string'
    },
    message: {
      type: 'string'
    },
  },
  example: {
    statusCode: 401,
    error: 'Unauthorized',
    message: 'Unauthorized Token Error',
  }
}

const code409 = {
  description: 'Failure response',
  type: 'object',
  properties: {
    id: {
      type: 'string'
    },
    statusCode: {
      type: 'number'
    },
    error: {
      type: 'string'
    },
    status: {
      type: 'string'
    },
    message: {
      type: 'string'
    },
  },
  example: {
    id: randomID('failure'),
    status: 'fail',
    error: 'Conflict',

    message: 'Error Message',
  }
}

const code200User = {
  description: 'Successful response',
  type: 'object',
  example: {
    id: randomID('request'),
    status: 'success',
    docs: [{
      "_id": md5(new Date() + 2000),
      "email": "email@domain.com",
    }]
  }
}

const code200ProductList = {
  description: 'Successful response',
  type: 'object',
  properties: {
    id: {
      type: 'string'
    },
    status: {
      type: 'string'
    },
    list: {
      type: 'array',
      items: {
        type: 'object',
        properties: {
          _id: {
            type: 'string'
          },
          name: {
            type: 'string'
          },
          price: {
            type: 'string'
          },
          stock: {
            type: 'object',
            properties: {
              current: {
                type: 'number'
              },
              hold: {
                type: 'number'
              },
              taken: {
                type: 'number'
              },
            }
          },
        }
      }


    },
  },
  example: {
    id: randomID('request'),
    status: 'success',
    list: [{
      name: 'Product A',
      price: 20,
      stock: {
        current: 10,
        hold: 20,
        taken: 1
      }
    }]
  }
}


const code200GetCart = {
  description: 'Successful response',
  type: 'object',
  properties: {
    id: {
      type: 'string'
    },
    status: {
      type: 'string'
    },
    items: {
      type: 'array',
    },
    quantity: {
      type: 'array',
    },
    product: {
      type: 'array',
    },
  },
  example: {
    id: randomID('request'),
    status: 'success',
    list: [{
      _id: 'Product A',
      quantity: 20,
    }]
  }
}



const code200Message = {
  description: 'Successful response',
  type: 'object',
  properties: {
    id: {
      type: 'string'
    },
    status: {
      type: 'string'
    },
    message: {
      type: 'string'
    },
  },
  example: {
    id: randomID('request'),
    status: 'success',
    message: 'Message for operation',
  }
}

const code200Token = {
  description: 'Successful response',
  type: 'object',
  properties: {
    id: {
      type: 'string'
    },
    status: {
      type: 'string'
    },
    user: {
      type: 'object',
      properties: {
        _id: {
          type: 'string'
        },
        email: {
          type: 'string'
        },
        token: {
          type: 'string'
        },
      }
    },
  },
  example: {
    id: randomID('request'),
    status: 'success',
    user: {
      _id: md5(new Date() + 7000),
      token: Math.random().toString(36).substr(2) + Math.random().toString(36).substr(2) + Math.random().toString(36).substr(2) + Math.random().toString(36).substr(2) + Math.random().toString(36).substr(2) + Math.random().toString(36).substr(2) + Math.random().toString(36).substr(2) + Math.random().toString(36).substr(2) + Math.random().toString(36).substr(2) + Math.random().toString(36).substr(2) + Math.random().toString(36).substr(2) + Math.random().toString(36).substr(2) + Math.random().toString(36).substr(2) + Math.random().toString(36).substr(2) + Math.random().toString(36).substr(2) + Math.random().toString(36).substr(2),
      email: 'user@domain.com',
    }
  }
}




module.exports = {
  code400,
  code401,
  code409,
  code200Message,
  code200User,
  code200Token,
  code200ProductList,
  code200GetCart
};
