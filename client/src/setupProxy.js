const { createProxyMiddleware } = require('http-proxy-middleware');

module.exports = function(app) {
  app.use(
    '/v1',
    createProxyMiddleware({
      target: 'http://'+process.env.PROXYHOST+':3005',
      changeOrigin: true,
      logLevel: 'debug',
    })
  );

};