import {
    CART_DATA,
    CART_DATA_SUCCESS,
    CART_DATA_ERROR,
    CART_ITEM_UPDATE_DATA,
    PRODUCTS_DATA,
    PRODUCTS_DATA_SUCCESS,
    PRODUCTS_DATA_ERROR,
    PRODUCTS_ITEM_UPDATE,
    PRODUCTS_ITEM_STOCK_MODAL_CLOSE,
    PRODUCTS_SELECTED_PRODUCT
} from '../actions';


export const getCartList = (history) => ({
    type: CART_DATA,
    payload : { history}
});

export const getCartItemUpdate = (action,id,socket,history) => ({
    type: CART_ITEM_UPDATE_DATA,
    payload : { history, id, action, socket }
});

export const getCartProductList = (history) => ({
    type: PRODUCTS_DATA,
    payload : { history }
});

export const getProductItemUpdate = (product) => ({
    type: PRODUCTS_ITEM_UPDATE,
    payload : product
});


export const selectedProduct = (product) => ({
    type: PRODUCTS_SELECTED_PRODUCT,
    payload : product
});


export const closeStockModal = () => ({
    type: PRODUCTS_ITEM_STOCK_MODAL_CLOSE,
});

export const getCartProductsSuccess = (data) => ({
    type: PRODUCTS_DATA_SUCCESS,
    payload: { data }  
});

export const getCartProductsError = (error) => ({
    type: PRODUCTS_DATA_ERROR,
    payload: error
});

export const getCartSuccess = (data) => ({
    type: CART_DATA_SUCCESS,
    payload: { data} 
});

export const getCartError = (error) => ({
    type: CART_DATA_ERROR,
    payload: error
});
