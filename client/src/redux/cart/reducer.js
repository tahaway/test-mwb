import {
    CART_DATA,
    CART_DATA_SUCCESS,
    CART_DATA_ERROR,
	PRODUCTS_DATA,
	PRODUCTS_ITEM_UPDATE,
	PRODUCTS_DATA_SUCCESS,
	PRODUCTS_DATA_ERROR,
	PRODUCTS_ITEM_STOCK_MODAL_CLOSE,
	PRODUCTS_SELECTED_PRODUCT
} from '../actions';

const defaultChanges = {
	modal : false,
	item : {}
}

const INIT_STATE = {
	error: '',
	cart: {
		items : [],
		quantity : []
	},
	products : [],
	selectedProduct : {},
	changes : defaultChanges,
	loading: false,
};

// eslint-disable-next-line
export default (state = INIT_STATE, action) => {
	switch (action.type) {
		case CART_DATA:
			return { ...state, loading: true };
		case CART_DATA_SUCCESS:
			return { ...state,loading : true, cart : { items : action.payload.data.items, quantity : action.payload.data.quantity,  }};
		case CART_DATA_ERROR:
			return { ...state, loading: false };
		case PRODUCTS_DATA:
			return { ...state, loading: true };
		case PRODUCTS_DATA_SUCCESS:
			let selectedProduct = state.selectedProduct;
			if(state.selectedProduct._id){
			   let updateChanges = action.payload.data.list.filter(item=> item._id === state.selectedProduct._id);
			   selectedProduct = updateChanges[0];
			}
			return { ...state,loading : true, products : action.payload.data.list, selectedProduct : selectedProduct };
		case PRODUCTS_DATA_ERROR:
			return { ...state, loading: false };
		case PRODUCTS_ITEM_STOCK_MODAL_CLOSE:
			return { ...state, loading: true, changes : defaultChanges };
		case PRODUCTS_SELECTED_PRODUCT:
			return { ...state, loading: true, selectedProduct : action.payload };
		case PRODUCTS_ITEM_UPDATE:
			let products = state.products;
			let changes = defaultChanges
			let selectedProductUpdate = state.selectedProduct;
			if(action.payload.data._id){
				products = products.map(item=> {
					if(item._id === action.payload.data._id){
						return action.payload.data
					}
					return item
				});
				changes = action.payload.changes ? action.payload.changes : defaultChanges;
			}
			if(selectedProductUpdate._id){
				let updateChanges = products.filter(item=> item._id === selectedProductUpdate._id);
				selectedProductUpdate = updateChanges[0];
			}
			return { ...state,loading : true, products : products, changes : changes, selectedProduct : selectedProductUpdate };

		default: return { ...state };
	}
}
