import appLocaleData from 'react-intl/locale-data/et';
import etMessages from '../locales/et_EE';

const EtLang = {
    messages: {
        ...etMessages
    },
    locale: 'et-EE',
    data: appLocaleData
};
export default EtLang;