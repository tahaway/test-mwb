const axios = require('axios').default;

const Request = async (uri, type, data, auth) => {
    return new Promise((resolve, reject) => {  
        let apiLink = window.location.origin;
        let headers = false;
        if(!auth) headers = { headers : {'Authorization': 'Bearer ' + localStorage.getItem('token') }};
        if(type === 'get') resolve(axios.get(apiLink + uri,headers));
        if(type === 'post') resolve(axios.post(apiLink + uri,data,headers));
    });
}

export default Request;

