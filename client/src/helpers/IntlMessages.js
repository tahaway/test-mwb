import React from 'react';
import { FormattedMessage, injectIntl } from 'react-intl';

const InjectMassage = props => {
    if(props.text) return props.intl.formatMessage({id : props.id});
    return <FormattedMessage {...props} />
};

export default injectIntl(InjectMassage, {
    withRef: false
});


