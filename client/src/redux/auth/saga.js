
import { all, call, fork, put, takeEvery, takeLatest } from 'redux-saga/effects';
import {
    LOGIN_USER,
    REGISTER_USER,
    REFRESH_TOKEN_USER,
    LOGOUT_USER,
    FORGOT_PASSWORD,
    RESET_PASSWORD,
} from '../actions';

import {
    loginUserSuccess,
    loginUserError,
    registerUserSuccess,
    registerUserError,
    forgotPasswordSuccess,
    forgotPasswordError,
    resetPasswordSuccess,
    resetPasswordError
} from './actions';
import Request  from '../api/request';
import ErrorHandler  from '../api/remove';


export function* watchLoginUser() {
    yield takeLatest(LOGIN_USER, loginWithEmailPassword);
}

export function* watchRefreshToken() {
    yield takeLatest(REFRESH_TOKEN_USER, refreshCurrentUserToken);
}


function* refreshCurrentUserToken({payload}) {

	try {
		const response = yield call(Request,'/v1/api/auth/access-web-token/','get');
        localStorage.setItem('token',response.data.data.access_token);
	} catch (error) {
        if(!ErrorHandler(error)){
			if(payload.history) payload.history.push('/user/login'); 
		} 
	}
}

const loginWithEmailPasswordAsync = async (email, password) =>
    await Request('/v1/api/auth/login/','post',{ email, password },true)
            .then(response => response.data)
            .catch(error => error );

function* loginWithEmailPassword({ payload }) {
    const { username, password } = payload.user;
    const { history } = payload;

    try {
        const loginUser = yield call(loginWithEmailPasswordAsync, username, password);
        if(loginUser.status === 'success'){
             localStorage.setItem('token', loginUser.user.token);
             yield put(loginUserSuccess(loginUser.user.token,loginUser.user._id));
             history.push('/');
        }else{
            yield put(loginUserError(loginUser.message));
        }
        
    } catch (error) {
        yield put(loginUserError(error));

    }
}


export function* watchRegisterUser() {
    yield takeEvery(REGISTER_USER, registerWithEmailPassword);
}

const registerWithEmailPasswordAsync = async (email, password) => {

}

function* registerWithEmailPassword({ payload }) {
    const { email, password } = payload.user;
    const { history } = payload
    try {
        const registerUser = yield call(registerWithEmailPasswordAsync, email, password);
        if (!registerUser.message) {
            localStorage.setItem('token', registerUser.user.uid);
            yield put(registerUserSuccess(registerUser));
            history.push('/')
        } else {
            yield put(registerUserError(registerUser.message));

        }
    } catch (error) {
        yield put(registerUserError(error));
    }
}



export function* watchLogoutUser() {
    yield takeEvery(LOGOUT_USER, logout);
}

const logoutAsync = async (history) => {
    history.push('/')
}

function* logout({ payload }) {
    const { history } = payload
    try {
        yield call(logoutAsync, history);
        localStorage.removeItem('token');
    } catch (error) {
    }
}

export function* watchForgotPassword() {
    yield takeEvery(FORGOT_PASSWORD, forgotPassword);
}


function* forgotPassword({ payload }) {
    const { email } = payload.forgotUserMail;
    try {
		let uri = '/v1/api/forgot/';
		const forgotPasswordStatus = yield call(Request,uri,'post',{ email });
         if (forgotPasswordStatus.data.response === 'false') {
            yield put(forgotPasswordError(forgotPasswordStatus.data.message));
        }else{
            yield put(forgotPasswordSuccess(forgotPasswordStatus.data.response));
        }

	} catch (error) {
		if(!ErrorHandler(error)){
			if(payload.history) payload.history.push('/user/login'); 
		} 
        yield put(forgotPasswordError(error));
	}
}

export function* watchResetPassword() {
    yield takeEvery(RESET_PASSWORD, resetPassword);
}


function* resetPassword({ payload }) {
    const { username, newPassword, currentPassword } = payload;
    try {
        let uri = '/v1/api/auth/change/password/';
		const resetPasswordStatus = yield call(Request,uri,'post',{ username : username, password : currentPassword, new : newPassword });
          if (resetPasswordStatus.data.response === 'false') {
            yield put(resetPasswordError(resetPasswordStatus.data.message));
        }else{
            yield put(resetPasswordSuccess(resetPasswordStatus.data.response));
        }

    } catch (error) {
        yield put(resetPasswordError(true));

    }
}

export default function* rootSaga() {
    yield all([
        fork(watchLoginUser),
        fork(watchLogoutUser),
        fork(watchRefreshToken),
        fork(watchRegisterUser),
        fork(watchForgotPassword),
        fork(watchResetPassword),
    ]);
}