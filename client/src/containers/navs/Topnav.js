import React, { Component } from "react";
import { injectIntl } from "react-intl";
import { Dropdown } from 'react-bootstrap';
import IntlMessages from "../../helpers/IntlMessages";
import {
  UncontrolledDropdown,
  DropdownItem,
  DropdownToggle,
  DropdownMenu,
} from "reactstrap";

import { NavLink } from "react-router-dom";
import { connect } from "react-redux";

import {
  setContainerClassnames,
  clickOnMobileMenu,
  logoutUser,
  changeLocale,
  resetPassword
} from "../../redux/actions";

import {
  menuHiddenBreakpoint,
  searchPath,
  isDarkSwitchActive
} from "../../constants/defaultValues";

import { MobileMenuIcon, MenuIcon } from "../../components/svg";
import TopnavDarkSwitch from "./Topnav.DarkSwitch";

import { getDirection, setDirection } from "../../helpers/Utils";

class TopNav extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isInFullScreen: false,
      searchKeyword: "",
      modal: false,
      current  : "",
      new : "",
      check : "",
      message : ""
    };
    this.validate = this.validate.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }


  toggle = () => {
    this.setState(prevState => ({
      modal: !prevState.modal
    }));
  };

  componentDidUpdate(prevProps,prevState) {
      if (this.props.error && prevProps.error !== this.props.error) {
        this.setState({
              ...this.state,
              error : <IntlMessages id="menu.change.password.error">{(error)=>error}</IntlMessages>
        });
    } else {
      if (!this.props.loading && this.props.newPassword === "success" && prevProps.newPassword !== this.props.newPassword){
        this.setState({
              ...this.state,
              modal : false,
        });
      }
    }
       
  }


  handleChangeLocale = (locale, direction) => {
    this.props.changeLocale(locale);

    const currentDirection = getDirection().direction;
    if (direction !== currentDirection) {
      setDirection(direction);
      setTimeout(() => {
        window.location.reload();
      }, 500);
    }
  };

  isInFullScreen = () => {
    return (
      (document.fullscreenElement && document.fullscreenElement !== null) ||
      (document.webkitFullscreenElement &&
        document.webkitFullscreenElement !== null) ||
      (document.mozFullScreenElement &&
        document.mozFullScreenElement !== null) ||
      (document.msFullscreenElement && document.msFullscreenElement !== null)
    );
  };
  handleSearchIconClick = e => {
    if (window.innerWidth < menuHiddenBreakpoint) {
      let elem = e.target;
      if (!e.target.classList.contains("search")) {
        if (e.target.parentElement.classList.contains("search")) {
          elem = e.target.parentElement;
        } else if (
          e.target.parentElement.parentElement.classList.contains("search")
        ) {
          elem = e.target.parentElement.parentElement;
        }
      }

      if (elem.classList.contains("mobile-view")) {
        this.search();
        elem.classList.remove("mobile-view");
        this.removeEventsSearch();
      } else {
        elem.classList.add("mobile-view");
        this.addEventsSearch();
      }
    } else {
      this.search();
    }
  };
  addEventsSearch = () => {
    document.addEventListener("click", this.handleDocumentClickSearch, true);
  };
  removeEventsSearch = () => {
    document.removeEventListener("click", this.handleDocumentClickSearch, true);
  };

  handleDocumentClickSearch = e => {
    let isSearchClick = false;
    if (
      e.target &&
      e.target.classList &&
      (e.target.classList.contains("navbar") ||
        e.target.classList.contains("simple-icon-magnifier"))
    ) {
      isSearchClick = true;
      if (e.target.classList.contains("simple-icon-magnifier")) {
        this.search();
      }
    } else if (
      e.target.parentElement &&
      e.target.parentElement.classList &&
      e.target.parentElement.classList.contains("search")
    ) {
      isSearchClick = true;
    }

    if (!isSearchClick) {
      const input = document.querySelector(".mobile-view");
      if (input && input.classList) input.classList.remove("mobile-view");
      this.removeEventsSearch();
      this.setState({
        searchKeyword: ""
      });
    }
  };
  handleSearchInputChange = e => {
    this.setState({
      searchKeyword: e.target.value
    });
  };
  handleSearchInputKeyPress = e => {
    if (e.key === "Enter") {
      this.search();
    }
  };

  search = () => {
    this.props.history.push(searchPath + "/" + this.state.searchKeyword);
    this.setState({
      searchKeyword: ""
    });
  };

  toggleFullScreen = () => {
    const isInFullScreen = this.isInFullScreen();

    var docElm = document.documentElement;
    if (!isInFullScreen) {
      if (docElm.requestFullscreen) {
        docElm.requestFullscreen();
      } else if (docElm.mozRequestFullScreen) {
        docElm.mozRequestFullScreen();
      } else if (docElm.webkitRequestFullScreen) {
        docElm.webkitRequestFullScreen();
      } else if (docElm.msRequestFullscreen) {
        docElm.msRequestFullscreen();
      }
    } else {
      if (document.exitFullscreen) {
        document.exitFullscreen();
      } else if (document.webkitExitFullscreen) {
        document.webkitExitFullscreen();
      } else if (document.mozCancelFullScreen) {
        document.mozCancelFullScreen();
      } else if (document.msExitFullscreen) {
        document.msExitFullscreen();
      }
    }
    this.setState({
      isInFullScreen: !isInFullScreen
    });
  };

  handleLogout = () => {
    this.props.logoutUser(this.props.history);
  };

  menuButtonClick = (e, menuClickCount, containerClassnames) => {
    e.preventDefault();

    setTimeout(() => {
      var event = document.createEvent("HTMLEvents");
      event.initEvent("resize", false, false);
      window.dispatchEvent(event);
    }, 350);
    this.props.setContainerClassnames(
      ++menuClickCount,
      containerClassnames,
      this.props.selectedMenuHasSubItems
    );
  };
  mobileMenuButtonClick = (e, containerClassnames) => {
    e.preventDefault();
    this.props.clickOnMobileMenu(containerClassnames);
  };

  changeLanguage = lang => {
    localStorage.setItem('currentLanguage',lang);
    window.location.reload(false);
  }

  validateFieldCurrent( value) {
    let error;
    if (!this.state.current) {
      error = <IntlMessages id="view.change.passwor.current.enter" />;
    } else if (this.state.current.length < 8) {
      error = <IntlMessages id="view.change.passwor.current.message" />;
    }
    return error;
  }

  validateFieldNew(value) {
    let error;
    if (!this.state.new) {
      error = <IntlMessages id="view.change.passwor.new.enter" />;
    } else if (this.state.new.length < 8) {
      error = <IntlMessages id="view.change.passwor.new.message" />;
    }
    return error;
  }

  validateFieldCheck(value) {
    let error;
    if (!this.state.new) {
      error = <IntlMessages id="view.change.passwor.check.enter" />;
    } else if (this.state.check.length < 8) {
      error = <IntlMessages id="view.change.passwor.check.message" />;
    }else if (this.state.current !== this.state.check) {
      error = <IntlMessages id="view.change.passwor.check.notEqual" />;
    }
    return error;
  }

  handleSubmit(values) {
    this.props.resetPassword({
      username : values.username,
      currentPassword : values.current,
      newPassword : values.new,  
      history : this.props.history
    })
  }

    validate(values) {
        let errors = {}

        if (!values.username) {
            errors.username = <IntlMessages id="view.change.passwor.username.enter" />;
        } else if (values.username.length < 4) {
            errors.username = <IntlMessages id="view.change.passwor.username.message" />;
        }

        if (!values.current) {
            errors.current = <IntlMessages id="view.change.passwor.current.enter" />;
        } else if (values.current.length < 8) {
            errors.current = <IntlMessages id="view.change.passwor.current.message" />;
        }

        if (!values.new) {
            errors.new = <IntlMessages id="view.change.passwor.new.enter" />;
        } else if (values.new.length < 8) {
            errors.new = <IntlMessages id="view.change.passwor.new.message" />;
        }

        if (values.new === values.current) {
            errors.new = <IntlMessages id="view.change.passwor.new.same" />;
        } 

        return errors;
    }

  render() {
    const { containerClassnames, menuClickCount } = this.props;
    return (
      <nav className="navbar fixed-top">
        <div className="d-flex align-items-center navbar-left">
          <NavLink
            to="#"
            className="menu-button d-none d-md-block"
            onClick={e =>
              this.menuButtonClick(e, menuClickCount, containerClassnames)
            }
          >
            <MenuIcon />
          </NavLink>
          <NavLink
            to="#"
            className="menu-button-mobile d-xs-block d-sm-block d-md-none"
            onClick={e => this.mobileMenuButtonClick(e, containerClassnames)}
          >
            <MobileMenuIcon />
          </NavLink>

          <div className="d-inline-block">
          </div>
        </div>
        <a className="navbar-logo" href="/">
          <span className="logo-mobile d-block d-xs-none" />
        </a>
        <div className="navbar-right">

          {isDarkSwitchActive && <TopnavDarkSwitch />}
          <div className="header-icons d-inline-block align-middle">
            <button
              className="header-icon btn btn-empty d-none d-sm-inline-block"
              type="button"
              id="fullScreenButton"
              onClick={this.toggleFullScreen}
            >
              {this.state.isInFullScreen ? (
                <i className="simple-icon-size-actual d-block" />
              ) : (
                  <i className="simple-icon-size-fullscreen d-block" />
                )}
            </button>
          </div>
          <div className="d-inline-block align-middle">
              <Dropdown>
                      <Dropdown.Toggle 
                      variant="secondary btn-sm" 
                      id="dropdown-basic">
                          <IntlMessages id="language.title" />
                      </Dropdown.Toggle>

                      <Dropdown.Menu >
                          <Dropdown.Item onClick={ ()=> this.changeLanguage('en') } ><IntlMessages id="language.en" /></Dropdown.Item>
                          <Dropdown.Item onClick={ ()=> this.changeLanguage('et') }><IntlMessages id="language.et" /></Dropdown.Item>
                      </Dropdown.Menu>
                      </Dropdown>
          </div>
          <div className="user d-inline-block">
            <UncontrolledDropdown className="dropdown-menu-right">
              <DropdownToggle className="p-0" color="empty">
                <span>
                  <img alt="Profile" src="/assets/img/api.png" />
                </span>
              </DropdownToggle>
              <DropdownMenu className="mt-3" right>
                 <DropdownItem onClick={() => this.handleLogout()}>
                  <IntlMessages id="menu.sign.out" />
                </DropdownItem>
              </DropdownMenu>
            </UncontrolledDropdown>
          </div>
        </div>
      </nav>
    );
  }
}

const mapStateToProps = ({ menu, settings,authUser }) => {
    const { newPassword,error } = authUser;
  const { containerClassnames, menuClickCount, selectedMenuHasSubItems } = menu;
  const { locale } = settings;
  return {
    containerClassnames,
    menuClickCount,
    selectedMenuHasSubItems,
    locale,
    newPassword,
    error
  };
};
export default injectIntl(
  connect(
    mapStateToProps,
    { setContainerClassnames, clickOnMobileMenu, logoutUser, changeLocale, resetPassword }
  )(TopNav)
);
