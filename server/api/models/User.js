const _ = require('lodash');
const bcrypt = require('bcrypt-nodejs');
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const userSchema = new mongoose.Schema({
    _id: { type: Schema.ObjectId, auto: true },
    companyID : { type: Schema.ObjectId },
    username :  {type : String },
    password : {type : String },
    department :  {type : String },
    code : {type : String },
    email : {type : String},
    phone : {type : String},
    address : {type : String},
    notification : {type : Boolean, default : false }, 
    role :  { type: Number, enum: [0,1,2,3,4], default: 1 },
    access : {
        apiKey : {type : String },
        ip : { type: Array },
        permission : [[{type : Number }]],
    },
    active : {type : Boolean, default : false },
    deleted : {type : Boolean, default : false },
    reset : {type : Boolean, default : false },
    modifiedAt : {
        type: Date,
    },
});


/**
 * Password hash middleware.
 */
userSchema.pre('save', function save(next) {
    const user = this;
    if (!user.isModified('password')) { return next(); }
    bcrypt.genSalt(10, (err, salt) => {
      if (err) { return next(err); }
      bcrypt.hash(user.password, salt, null, (err, hash) => {
        if (err) { return next(err); }
        user.password = hash;
        user.modifiedAt =  Date.now();
        next();
      });
    });
});

userSchema.pre('findOneAndUpdate', function(next) {
  const update = this.getUpdate();
  if (!_.isEmpty(update.password)) {
    bcrypt.genSalt(10, (err, salt) => {
      bcrypt.hash(update.password, salt,null, (err, hash) => {
        this.getUpdate().password = hash;
        next();
      })
    })
  } else {
    next();
  }
});



  /**
   * Helper method for validating user's password.
   */
  userSchema.methods.comparePassword = function comparePassword(candidatePassword) {
    return new Promise((resolve, reject) => {
      bcrypt.compare(candidatePassword, this.password, (err, isMatch) => {
        if (err) { reject(err); }
        resolve(isMatch);
      });
    });
  };


const User = mongoose.model('users', userSchema);
User.hide = ['password','__v'];

module.exports = User;
