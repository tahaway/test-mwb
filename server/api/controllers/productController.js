const {
    success,
    error
} = require('../utils/general');
const {
    APPLICATION_ERROR
} = require('../models/Errors');
const Product = require('../models/Product');

const getProductList = async (req, res) => {
    try {
        const list = await Product.find().skip(req.params.limit * (req.params.page - 1)).limit(req.params.limit);
        return success({ list }, res);
    } catch (err) {
        return error(APPLICATION_ERROR);
    }
}


module.exports = {
    getProductList
}