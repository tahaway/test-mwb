const { SignUpResponse } = require('../models/Auth');
const { INVALID_PASSWORD, USER_DOESNT_EXISTS, USER_EXISTS } = require('../models/Errors');
const { VERIFICATION_EMAIL_SENT } = require('../models/Messages');
const { getRandomBytes } = require('../utils/crypto');
const { error,success } = require('../utils/general');
const nconf = require('nconf');
const User = require('../models/User');
const Carts = require('../models/Cart');
const { objectID  } = require('../utils/general');

const postLogin = async (req, res) => {
    const { email, password } = req.body;

    try {
        const user = await User.findOne({ email : email.toLowerCase().toString() }).exec();
        if (!user) return error(res,USER_DOESNT_EXISTS);
        const isMatch = await user.comparePassword(password);
        if(!isMatch)  return error(res,INVALID_PASSWORD);

        const { _id } = user;
        const token = await res.jwtSign({ _id }, { expiresIn: nconf.get('app.userJwtExpiry') });
        const response = { user : { email, token, _id } };
        return success(response,res);
    } catch (err) {
        return error(res);
    }
};

const postSignup = async (req, res) => {
    const { email, password } = req.body;
    try {
        const existingUser = await User.findOne({ email : req.body.email.toLowerCase().toString() });
        if (existingUser) return error(res,USER_EXISTS);
    
        const user = new User({ email, password, active : true });
        const newUser = await user.save();
        const cart = new Carts({ uid : objectID(newUser._id),  items : [], quantity : [] });
        await cart.save();

        const { _id } = newUser;
        const token = await res.jwtSign({ _id }, { expiresIn: nconf.get('app.userJwtExpiry') });
        return success({ user : { email: email, token, _id } },res)
        
    } catch (err) {
        return error(res);
    }
};

module.exports = {
    postLogin,
    postSignup,
};