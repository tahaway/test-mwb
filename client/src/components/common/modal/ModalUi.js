import React, { Component } from "react";
import {
  Modal,
} from "reactstrap";
import ModalBodyUI from "./ModalBody";

export default class ModalUi extends Component {

  render() {
    const { modal, toggle  } = this.props;
    return (
      <Modal isOpen={modal}  toggle={toggle}>
        <ModalBodyUI toggle={toggle} />
      </Modal>
    );
  }
}
