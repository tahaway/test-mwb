import React, { Component } from "react";
import { Colxx } from "../common/CustomBootstrap";
import { injectIntl } from "react-intl";
import { Card } from "reactstrap";
import { Link } from "react-router-dom";
import classnames from "classnames";
import { ContextMenuTrigger } from "react-contextmenu";

class Item extends Component {

    render() {
        const { item, index, toggle } = this.props
        return (
            <Colxx xxs="12" key={item._id} className="mb-3">
                  <ContextMenuTrigger id="menu_id" data={item._id}>
                    <Card
                      className={classnames("d-flex flex-row", {
                      })}
                    >
                    <div className="pl-2 d-flex flex-grow-1 min-width-zero">
                        <div className="card-body align-self-center d-flex flex-column flex-lg-row justify-content-between min-width-zero align-items-lg-center">
                            <Link to={"#"} onClick={()=> toggle(item)}>
                            {index + 1 + ".  "} {item.name}
                            </Link>
                        </div>
                        <div className={"custom-control custom-checkbox pl-1 align-self-center pr-4"}>
                        <input disabled max={5} className={"form-control w-30 float-right"} value={item.stock.current} />
                        </div>
                    </div>
                    </Card>
                  </ContextMenuTrigger>
            </Colxx>
        );
    }
}

export default injectIntl(Item);
