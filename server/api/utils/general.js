var ObjectID = require('mongodb').ObjectID
const { MODULE_PERMISSION_BLOCK, MODULE_PERMISSION_ALERT, APPLICATION_ERROR } = require('../models/Errors');
let roles = ['disabled','admin','manager','billing','staff','monitor'];
let objectID = string => string ? ObjectID(string) : ObjectID() ;
let randomID = (string) => string +'-'+  Math.round(new Date().getTime()/1000) + '-' + Math.round((Math.random() * 36 ** 12)).toString(36);

const success = (docs,res) => {
    if(docs){
        if(docs._doc){
            docs = { docs : docs._doc, total : 1 }
        }
    }
    if(!docs){
        docs = {
            ...docs,
            total : 0,
        }
    }
    return res.send({ id : randomID('request'), status : 'success', ...docs });
} 
const error = (res,tag,code) => {
    res.code(code ? code : 409);
    return res.send({ id : randomID('failure'), statusCode : code ? code : 409, error : 'Conflict', status : 'fail', message : tag ? tag :  APPLICATION_ERROR });
}


const converForSchemaSwagger = (obj,skip) => {
     Object.keys(obj).forEach(key => {
       console.log(obj[key]);
    });
}

const GUID = ()=> { // Public Domain/MIT
    var d = new Date().getTime();//Timestamp
    var d2 = Date.now()*1000;//Time in microseconds since page-load or 0 if unsupported
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = Math.random() * 16;//random number between 0 and 16
        if(d > 0){//Use timestamp until depleted
            r = (d + r)%16 | 0;
            d = Math.floor(d/16);
        } else {//Use microseconds since page-load if supported
            r = (d2 + r)%16 | 0;
            d2 = Math.floor(d2/16);
        }
        return (c === 'x' ? r : (r & 0x3 | 0x8)).toString(16);
    });
}

module.exports = {
  objectID,
  success,
  error,
  GUID,
  randomID,
  converForSchemaSwagger,
};
