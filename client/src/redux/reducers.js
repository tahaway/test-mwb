import { combineReducers } from 'redux';
import settings from './settings/reducer';
import menu from './menu/reducer';
import authUser from './auth/reducer';
import cartapp from './cart/reducer';


const reducers = combineReducers({
  menu,
  settings,
  authUser,
  cartapp,

});

export default reducers;