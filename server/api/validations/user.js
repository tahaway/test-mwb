const nconf = require('nconf');
const userPasswordRegex = nconf.get('userPasswordRegex');
const {
  randomID,
  converForSchemaSwagger
} = require('../utils/general');
const {
  code400,
  code401,
  code409,
  code200User,
  code200UserList,
  code200Message
} = require('../../config/Swagger');
const md5 = require('md5');

const validateUserList = {
  schema: {
    description: 'Access your user accounts.',
    tags: ['user'],
    params: {
      type: 'object',
      properties: {
        page: {
          type: 'number',
          default: 1,
        },
        limit: {
          type: 'number',
          default: 10,
        },
      },
      required: ['page', 'limit'],
    },
    response: {
      400: code400,
      401: code401,
      200: code200UserList,
      409: code409
    },
  },

};



module.exports = {
  validateUserList,
};
