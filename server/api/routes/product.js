const {  getProductList  } = require('../controllers/productController')
const {  validateProductList  } = require('../validations/product');

module.exports = async function (fastify,opts) {

    fastify.route({
        method: 'GET',
        url: '/v1/api/product/list/:page/:limit/',
        schema : validateProductList.schema,
        preHandler: fastify.auth([
            fastify.asyncVerifyJWT,     
        ]),
        handler : getProductList
    })

  }