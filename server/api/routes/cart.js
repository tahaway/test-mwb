const request = require('request');
const {  getCart, updateCart  } = require('../controllers/cartController')
const {  validateGetCart, validateUpdateCart  } = require('../validations/cart');

module.exports = async function (fastify,opts) {

    fastify.route({
        method: 'GET',
        url: '/v1/api/cart/',
        schema : validateGetCart.schema,
        preHandler: fastify.auth([
            fastify.asyncVerifyJWT,     
        ]),
        handler : getCart
    })

    fastify.route({
        method: 'GET',
        url: '/v1/api/cart/:action/:id/',
        schema : validateUpdateCart.schema,
        preHandler: fastify.auth([
            fastify.asyncVerifyJWT,     
        ]),
        handler :  (request,response) => updateCart(request,response,fastify)
    })
    

  }