const {
    objectID
} = require('../utils/general');
const Carts = require('../models/Cart');
const Products = require('../models/Product');

module.exports = async (server) => {
    server.ready(err => {
        if (err) throw err

        console.log('Server started.')
        server.io
            .on('connection', socket => {

                let tokenInfo = false;
                if (!socket.handshake.query.token) {
                    socket.disconnect();
                }

                if (socket.handshake.query.token !== 'false') {
                    try {
                        tokenInfo = server.jwt.verify(socket.handshake.query.token)
                    } catch (error) {
                        socket.disconnect();
                    }
                }

                socket.on('subscribe', function (room) {
                    try {
                        console.log('[socket]','User :',socket.id ,'join room :', room)
                        socket.join(room);
                        socket.to(room).emit('user joined', socket.id);
                    } catch (e) {
                        console.log('[error]', 'User :',socket.id ,'join room :', e);
                        socket.emit('error', 'couldnt perform requested action');
                    }
                })

                socket.on('updateProduct', data => {
                    socket.to('cart').emit("updateProduct", data);
                })

                socket.on('unsubscribe', function (room) {
                    try {
                        console.log('[socket]','User :',socket.id , 'leave room :', room);
                        socket.leave(room);
                        socket.to(room).emit('user left', socket.id);
                    } catch (e) {
                        console.log('[error]', 'User :',socket.id ,'leave room :', e);
                        socket.emit('error', 'couldnt perform requested action');
                    }
                })

                socket.on('disconnect', async () => {
                    if (tokenInfo._id) {

                        let cartList = await Carts.findOneAndUpdate({
                            uid: objectID(tokenInfo._id)
                        }, {
                            items: [],
                            quantity: []
                        });

                        cartList.items.forEach(async (item, index) => {
                            let removeQuantiy = parseInt(cartList.quantity[index] ? cartList.quantity[index] : 0);
                            let query = {
                                $inc: {
                                    "stock.current": removeQuantiy
                                }
                            };
                            await Products.updateOne({
                                    _id: objectID(item)
                                },
                                query);
                            socket.to('cart').emit("updateCart", true);
                        });

                        console.log(`Socket ${socket.id} cart removed. UserID : `, tokenInfo._id);
                    }
                    console.log(`Socket ${socket.id} disconnected.`);
                });

            })
    })
};
