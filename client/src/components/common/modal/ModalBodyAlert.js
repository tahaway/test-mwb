import React, { Component } from "react";
import { connect } from "react-redux";
import { injectIntl } from "react-intl";
import {
  Button,
  ModalHeader,
  ModalBody,
  ModalFooter,
} from "reactstrap";

class ModalBodyAlert extends Component {

  render() {
    const { data, toggle  } = this.props;
    const action = data.action === 'addition' ? 'decreased' : 'increased';
    return (
      <div>
        <ModalHeader toggle={toggle}>
            <strong>Stock Update</strong>
        </ModalHeader>
        <ModalBody> {data.name} quantity {action} by {data.quantity} </ModalBody>
        <ModalFooter> 
            <Button onClick={toggle} color="danger" className="default mb-2">
                Close
            </Button> 
        </ModalFooter>
      </div>
    );
  }
}

const mapStateToProps = ({ cartapp }) => {
  return { cartapp };
};

export default injectIntl(
  connect(
    mapStateToProps,
    {
        
    }
  )(ModalBodyAlert)
);