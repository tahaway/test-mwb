const APPLICATION_ERROR = "API facing problem to handle this request.";
const USER_EXISTS = 'Email address already exists.';
const INVALID_PASSWORD = 'Invalid Password.';
const USER_DOESNT_EXISTS = 'User doesn\'t exist.';
const NOT_FOUND = "Record not found.";
const INVALID_TOKEN = 'Invalid or Expired Token.';
const NOTHING_UPDATE = "Nothing to update.";


module.exports = {
  USER_EXISTS,
  INVALID_PASSWORD,
  USER_DOESNT_EXISTS,
  INVALID_TOKEN,
  NOTHING_UPDATE,
  APPLICATION_ERROR,
  NOT_FOUND
};
