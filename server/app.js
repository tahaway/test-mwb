const nconf = require('nconf');
const server = require('./server')
const { connectMongo } = require('./api/db/db')
const { loadSettings } = require('./config/configurationAdaptor')

const appSettingsPath = "./config/appSettings.json"

loadSettings({ appSettingsPath })
    .then(() => {
        
        const { URI  } = process.env;
        connectMongo(URI);

        // Read the config property required for starting the server
        const serverOptions = {
            logSeverity: nconf.get('logSeverity'),
        };
        server.createServer(serverOptions);
        // TODO Start the server
    })
    .catch((err) => {
        console.log(err);
    })