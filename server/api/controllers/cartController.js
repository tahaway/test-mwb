const {
    success,
    error
} = require('../utils/general');
const {
    APPLICATION_ERROR,
    NOT_FOUND
} = require('../models/Errors');
const Cart = require('../models/Cart');
const Products = require('../models/Product');
const { objectID  } = require('../utils/general');

const getCart = async (req, res) => {
    try {
        const list = await Cart.findOne({ uid : objectID(req.user._id) });
        return success({ items : list ? list.items : [], quantity : list ? list.quantity : [] }, res);
    } catch (err) {
        return error(APPLICATION_ERROR);
    }
}

const updateCart = async (req, res, fastify) => {
    try {
        const selectedProduct = await Products.find({ _id : objectID(req.params.id) });
        if(!selectedProduct) error(NOT_FOUND);
        let updateList = false;
        await Cart.findOne({ uid : objectID(req.user._id) }, async function(err, result) {

            if(result ){             

                let findIndex = result.items.indexOf(req.params.id);  
                if(req.params.action === 'addition'){
                    if(findIndex === -1){
                        result.items.push(req.params.id);
                        result.quantity.push(1);
                    }else{
                        result.quantity[ findIndex ] = parseInt(result.quantity[ findIndex ]) + 1;
                    }
                }
                
                if(req.params.action === 'subtract'){
                    if(findIndex !== -1){
                        if(result.quantity[ findIndex ] > 0){
                            result.quantity[ findIndex ] = parseInt(result.quantity[ findIndex ]) - 1;
                        }
                    }
                }
                
                updateList = await Cart.findOneAndUpdate({ uid : objectID(req.user._id) },
                    { items : result.items, quantity : result.quantity },
                    { new: true }
                )
                
                let valueHold = req.params.action === 'addition' ? 1 : -1;  
                valueHold = selectedProduct[0].stock.hold === 0 && req.params.action === 'subtract' ? 0 : valueHold;
                let valueCurrent = req.params.action === 'addition' ? -1 : 1;  

                let updatedProduct = await Products.findOneAndUpdate({ _id : objectID(req.params.id) },
                    { $inc : { "stock.hold" : valueHold, "stock.current" : valueCurrent, } },
                    { new : true}
                )

                return success({ items : updateList.items, quantity : updateList.quantity, product : [updatedProduct]  }, res);
            }
        })
    } catch (err) {
        return error(APPLICATION_ERROR);
    }
}


module.exports = {
    getCart,
    updateCart
}